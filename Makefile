run:
	poetry run uvicorn src.main:app --port 8030 --reload

lint: format
	poetry run pylint src

format: sort_imports
	poetry run black src

sort_imports:
	poetry run isort src

readme_html:
	mkdir -p docs/_build
	poetry run docutils README.rst docs/_build/README.html
