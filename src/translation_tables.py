"""
This file contains translation tables
"""


def reversed_dict(dct):
    return {dct[key]: key for key in dct}


TABLE_TO_LEET = dict(zip("OIEASBToieasbtОЕАБТЯоеабтя", "01345670134567034679034679"))
TABLE_FROM_LEET = reversed_dict(TABLE_TO_LEET)

TABLE_TO_FLIP_LOW = {
    "a": "\u0250",
    "b": "q",
    "c": "\u0254",
    "d": "p",
    "p": "d",
    "e": "\u01DD",
    "f": "\u025F",
    "g": "\u0183",
    "h": "\u0265",
    "i": "\u0131",
    "j": "\u027E",
    "k": "\u029E",
    "m": "\u026F",
    "n": "u",
    "r": "\u0279",
    "t": "\u0287",
    "v": "\u028C",
    "w": "\u028D",
    "y": "\u028E",
    ".": "\u02D9",
    "?": "\u00BF",
    "!": "\u00A1",
    "'": ",",
    "_": "\u203E",
    "а": "ɐ",
    "б": "ƍ",
    "в": "ʚ",
    "г": "ɹ",
    "д": "ɓ",
    "ё": "ǝ",
    "е": "ǝ",
    "з": "ε",
    "и": "и",
    "й": "ņ",
    "к": "ʞ",
    "л": "v",
    "м": "w",
    "н": "н",
    "о": "о",
    "п": "u",
    "р": "d",
    "с": "ɔ",
    "т": "ɯ",
    "у": "ʎ",
    "ф": "ф",
    "х": "х",
    "ц": "ǹ",
    "ч": "Һ",
    "ш": "m",
    "щ": "m",
    "ъ": "q",
    "ы": "ıq",
    "ь": "q",
    "э": "є",
    "ю": "oı",
    "я": "ʁ",
}
TABLE_FROM_FLIP_LOW = reversed_dict(TABLE_TO_FLIP_LOW)

TABLE_TO_TRANSLIT = {
    "А": "A",
    "Б": "B",
    "В": "V",
    "Г": "G",
    "Д": "D",
    "Е": "E",
    "Ё": "IO",
    "Ж": "ZH",
    "З": "Z",
    "И": "I",
    "Й": "Y",
    "К": "K",
    "Л": "L",
    "М": "M",
    "Н": "N",
    "О": "O",
    "П": "P",
    "Р": "R",
    "С": "S",
    "Т": "T",
    "У": "U",
    "Ф": "F",
    "Х": "KH",
    "Ц": "C",
    "Ч": "CH",
    "Ш": "SH",
    "Щ": "W",
    "Ъ": "'",
    "Ы": "Y",
    "Ь": "'",
    "Э": "E",
    "Ю": "IU",
    "Я": "IA",
    "а": "a",
    "б": "b",
    "в": "v",
    "г": "g",
    "д": "d",
    "е": "e",
    "ё": "io",
    "ж": "zh",
    "з": "z",
    "и": "i",
    "й": "y",
    "к": "k",
    "л": "l",
    "м": "m",
    "н": "n",
    "о": "o",
    "п": "p",
    "р": "r",
    "с": "s",
    "т": "t",
    "у": "u",
    "ф": "f",
    "х": "kh",
    "ц": "c",
    "ч": "ch",
    "ш": "sh",
    "щ": "w",
    "ъ": "'",
    "ы": "y",
    "ь": "'",
    "э": "e",
    "ю": "iu",
    "я": "ia",
}
