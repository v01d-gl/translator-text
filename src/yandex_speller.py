"""Yandex.Speller moduler for TT
This modules provides Yandex.Speller API functional
"""


import requests as req
from requests.exceptions import RequestException


IGNORE_UPPERCASE = 1
IGNORE_DIGITS = 2
IGNORE_URLS = 4
IGNORE_CAPITALIZATION = 512
IGNORE_ROMAN_NUMERALS = 2048
FIND_REPEAT_WORDS = 8
IGNORE_LATIN = 16
NO_SUGGEST = 32
FLAG_LATIN = 128
BY_WORDS = 256

QUERY_OPTIONS = "options"
QUERY_LANGUAGES = "lang"
QUERY_FORMAT = "format"
QUERY_ENCODING = "ie"
QUERY_TEXT = "text"

API_URI = "https://speller.yandex.net/services/spellservice.json/"
ENDPOINT_CHECKTEXT = "checkText"


class Speller:
    """Yandex.Speller API class"""

    def __init__(self, text="", **kwargs):
        self.options = kwargs.get("options", IGNORE_URLS + IGNORE_ROMAN_NUMERALS)
        self.lang = kwargs.get("lang", "ru,en")
        self.format = kwargs.get("format", "plain")
        self.encoding = kwargs.get("encoding", "utf-8")
        self.text = text
        self.mistakes = {}

    def __repr__(self):
        return "<TTAPI::Yandex.Speller>"

    def correct(self) -> None:
        """Corrects mistakes in self.text"""
        txt = self.text

        for mistake in self.mistakes:
            word, solutions = mistake["word"], mistake["s"]
            if solutions:
                txt = txt.replace(word, "/".join(solutions))

        self.text = txt

    def check(self) -> bool:
        """
        Checks if any mistakes in self.text
        :return: True if there are mistakes else False
        """

        try:
            self.mistakes = req.post(  # pylint: disable=missing-timeout
                API_URI + ENDPOINT_CHECKTEXT,
                {
                    QUERY_ENCODING: self.encoding,
                    QUERY_FORMAT: self.format,
                    QUERY_LANGUAGES: self.lang,
                    QUERY_OPTIONS: self.options,
                    QUERY_TEXT: self.text,
                },
            ).json()
        except RequestException as e:
            raise SpellerRequestException("Failed to spell the words to Yandex") from e

        return bool(self.mistakes)


class SpellerRequestException(RequestException):
    """Custom requests.exceptions.RequestException
    If sth is wrong with request you will catch it
    """

    def __init__(self, cause="RequestFailed"):
        super().__init__()
        self.cause = cause

    def __repr__(self):
        return f"<TTAPI::Yandex.Speller::{self.cause}>"
