from fastapi import FastAPI
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles

from src.api import api
from src.config import STATIC_FOLDER


app = FastAPI()
app.mount("/static", StaticFiles(directory=STATIC_FOLDER), name="static")
app.mount("/api", api)


@app.get("/")
def index():
    return FileResponse(STATIC_FOLDER / "index.html")
