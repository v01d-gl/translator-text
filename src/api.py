"""This is TranslatorTextOpenAPI"""


from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse
from pydantic import BaseModel, validator

from src.modes import mode_dict


TUPLE_MODE_LIST = tuple(mode_dict.keys())


class TranslationRequest(BaseModel):
    text: str
    mode: str

    @validator("mode")
    def mode_must_be_in_mode_list(cls, mode):  # pylint: disable=no-self-argument
        if mode in TUPLE_MODE_LIST:
            return mode

        raise ValueError(f"{mode} not in {TUPLE_MODE_LIST}")


api = FastAPI()
api.add_middleware(CORSMiddleware, allow_origins=("*",), allow_methods=("*",), allow_headers=("*",))


@api.get("/")
def api_index():
    return RedirectResponse("docs")


@api.get("/mode-list")
def mode_list():
    return TUPLE_MODE_LIST


@api.post("/translate")
def translate(translation_request: TranslationRequest):
    return mode_dict[translation_request.mode](translation_request.text)
