"""TTAPI modes
Each function takes <text> param
and returns a dict of str result and dict options

Example:
>>> DATA = {'result': 'result_string', 'options': {'opt1': 'val1'}}
"""
from collections.abc import Callable
from functools import wraps

from pydantic import BaseModel

from src.translation_tables import (
    TABLE_FROM_FLIP_LOW,
    TABLE_FROM_LEET,
    TABLE_TO_FLIP_LOW,
    TABLE_TO_LEET,
    TABLE_TO_TRANSLIT,
)
from src.yandex_speller import Speller, SpellerRequestException


class TranslationOptions(BaseModel):
    example: bool


class TranslationResponse(BaseModel):
    result: str
    options: TranslationOptions


def default_format(mode: Callable, example: bool = False):
    """Default format decorator"""

    @wraps(mode)
    def decorator(text):
        return TranslationResponse(result=mode(text), options=TranslationOptions(example=example))

    return decorator


def replace(text: str, table: dict) -> str:
    """Replaces table's keys to values in text"""
    return "".join((table.get(key, key) for key in text))


def error(cause: str):
    return f"<TTError: {cause}>"


@default_format
def to_bin(text):
    return "".join(bin(char)[2:].zfill(8) for char in text.encode())


@default_format
def from_bin(text):
    if len(text) % 8:
        return error("Should be divisible by 8")

    chars = bytearray()
    for i in range(0, len(text), 8):
        byte_as_string = text[i : i + 8]
        try:
            char = int(byte_as_string, 2)
        except ValueError:
            return error(f"Invalid char {byte_as_string}")
        chars.append(char)

    try:
        return chars.decode()
    except UnicodeDecodeError:
        return error("Invalid unicode")


def strikethrough(text):
    return {
        "result": "&#0822;".join(tuple(text)),
        "options": {
            "example": True,
        },
    }


@default_format
def reverse(text):
    return text[::-1]


@default_format
def to_leet(text):
    return replace(text, TABLE_TO_LEET)


@default_format
def from_leet(text):
    return replace(text, TABLE_FROM_LEET)


@default_format
def to_flip(text):
    return replace(text, TABLE_TO_FLIP_LOW)


@default_format
def from_flip(text):
    return replace(text, TABLE_FROM_FLIP_LOW)


@default_format
def yandex_speller(text):
    speller = Speller(text)
    try:
        speller.check()
    except SpellerRequestException as exc:
        return error(exc.cause)
    speller.correct()

    return speller.text


@default_format
def to_translit(text):
    return replace(text, TABLE_TO_TRANSLIT)


mode_dict = {
    "to_bin": to_bin,
    "from_bin": from_bin,
    "strikethrough": strikethrough,
    "reverse": reverse,
    "to_leet": to_leet,
    "from_leet": from_leet,
    "to_flip": to_flip,
    "from_flip": from_flip,
    "yandex_speller": yandex_speller,
    "to_translit": to_translit,
}
