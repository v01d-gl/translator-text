/*
Copyright 2018 v01d

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


const DEFAULT_ELEM = 'body';

const _std_gradient_config = {
    grad_coeff: [1, 3, 2],
    max: [255, 255, 255],
    min: [0, 0, 0],
    init_color: [50, 27, 10],
    delay: 10,
    scene: 'background',
};


class Gradient {
    constructor(mnt_elem_or_config = DEFAULT_ELEM, config_or_none = _std_gradient_config) {
        let config, mnt_elem;
        if (typeof mnt_elem_or_config === 'string') {
            mnt_elem = mnt_elem_or_config;
            config = config_or_none;
        } else {
            config = mnt_elem_or_config;
            mnt_elem = DEFAULT_ELEM;
        }

        this.__render_loop_timer_id = null;
        this.grad_coeff = config.grad_coeff || _std_gradient_config.grad_coeff;
        this.delay = config.delay || _std_gradient_config.delay;
        this.scene = config.scene || _std_gradient_config.scene;

        this.min = config.min || _std_gradient_config.min;
        this.max = config.max || _std_gradient_config.max;
        this.color = config.init_color || _std_gradient_config.init_color;
        for (let i = 0; i < 3; i++) {
            if (this.min[i] > this.max[i])
                throw new RangeError('Min value cannot be more than Max');

            if (this.color[i] < this.min[i] || this.color[i] > this.max[i])
                this.color[i] = (this.min[i] + this.max[i]) / 2
        }

        this.elem = get_element_smart(mnt_elem);
    }


    start() {
        const self = this;

        if (self.__render_loop_timer_id !== null)
            throw new Error('Cannot be started twice. At first stop it.');

        function render_loop() {
            self.__render();
            self.__next();

            self.__render_loop_timer_id = setTimeout(render_loop, self.delay);
        }

        render_loop();
    }

    stop() {
        clearTimeout(this.__render_loop_timer_id);
        this.__render_loop_timer_id = null;
    }

    __render() {
        this.elem.style[this.scene] = `rgb(${this.color[0]}, ${this.color[1]}, ${this.color[2]})`;
    }

    __next() {
        for (let i = 0; i < 3; i++) {
            const pos = this.color[i] + this.grad_coeff[i];

            if (pos > this.max[i] || pos < this.min[i])
                this.grad_coeff[i] = -this.grad_coeff[i];

            this.color[i] += this.grad_coeff[i];
        }
    }
}


class BorderGradient extends Gradient {
    __render() {
        this.elem.style.border = `1px solid rgb(${this.color[0]}, ${this.color[1]}, ${this.color[2]})`;
    }
}


function get_element_smart(elem) {
    if (elem instanceof HTMLElement)
        return elem;
    else if (typeof elem === 'string')
        return document.querySelector(elem);
    else
        throw new Error('Could not find elem.');
}
