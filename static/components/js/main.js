const API_URI = '/api';


const body_grad = new Gradient({
    min: [185, 185, 198],
    max: [247, 248, 231],
    init_color: [185, 248, 200],
    grad_coeff: [1, 1, 1],
    delay: 81,
});
body_grad.start();


const _id = id => document.getElementById(id);
const
    input = _id('input'),
    output = _id('output'),
    example = _id('example'),
    about = _id('about'),
    mode = _id('mode'),
    btnMoveup = _id('moveup'),
    btnCopy = _id('copy');

let apiDelayTimerId;


(async () => {
    const response = await axios.get(`${API_URI}/mode-list`);
    const modeList = response.data;

    for (const [i, key] of modeList.entries()) {
        mode.options[i] = new Option(key);
    }

    $('#mode').material_select(() => {
        example.innerHTML = '';
        input.dispatchEvent(new Event('input'));
    });
})()


input.addEventListener('input', () => {
    if (apiDelayTimerId)
        clearTimeout(apiDelayTimerId);

    output.value = '...please, wait...';

    apiDelayTimerId = setTimeout(async () => {
        const response = await axios.post(`${API_URI}/translate`, {
            mode: mode.value,
            text: input.value,
        });

        const translation = await response.data;

        output.value = translation.result;

        if (translation.options.example) {
            example.innerHTML = translation.result;
        }
    }, 500);
});


btnMoveup.addEventListener('click', () => {
    input.value = output.value;
    input.dispatchEvent(new Event('input'));
});

btnCopy.addEventListener('click', () => {
    output.select();
    navigator.clipboard.writeText(getSelection().toString().trim());
});


about.addEventListener('click', function onAboutClick() {
    about.innerHTML = `
        (C) 2017 — ${(new Date).getFullYear()};<br/>
        <strong>
            Authors:
                <a href="mailto:v01d@v01d.ru" target="_self">v01d@v01d.ru</a>,
                <span>lilvein</span>,
                <a href="mailto:doomaykaka@gmail.com" target="_self">doomaykaka@gmail.com</a>
            <br/>
            Repositories:
                <a href="https://gitlab.com/v01d-gl/translator-text" target="_blank">Back, Front</a>,
                <a href="https://github.com/Doomaykaka/TranslatorText/" target="_blank">Android Client</a>
            <br/>
            # Just For Fun!
        </strong>
    `;
    about.removeEventListener('click', onAboutClick);
});
